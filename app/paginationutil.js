const utils = require('./utils');
const Markup = require('telegraf/markup');

/**
 *
 * Sets paginated numbers for pagination
 *
 * */
function getPagination( current, maxpage ) {

    var paginationText = [];
    if (current>1) paginationText.push(Markup.callbackButton(`«first`, '1'));
    if (current>2) paginationText.push(Markup.callbackButton(`‹prev`, (current-1).toString()));
    if (current + 1 === maxpage)  paginationText.push(Markup.callbackButton(`last»`, maxpage.toString()));
    if (!(current + 1 === maxpage) && current<maxpage)  paginationText.push(Markup.callbackButton(`next›`, (current+1).toString()));
    if (!(current + 1 === maxpage) && current<maxpage) paginationText.push(Markup.callbackButton(`last»`, maxpage.toString()));

    return Markup.inlineKeyboard([
        paginationText,
        [Markup.callbackButton('✅ Clear', 'delete')]
    ]);
}

function formatPartValue(i, paginatedText, partKeyMap, userHistoryText, maxLength) {

    var heading = paginatedText.substr(0, paginatedText.length-1);

    //TODO the below text will go to strings table
    const firstPageNavigation = "*{0} HISTORY* 💰💰💰\n\n Showing the first 5 {1}.\n\n Navigate to the next page to view more {1}.\n\n{2}";
    const nextPageNavigation = "*{0} HISTORY* 💰💰💰\n\n Showing 5 more {1}.\n\n Navigate to the next page to view more {1}.\n\n{2}";
    const lastPageNavigation = "*{0} HISTORY* 💰💰💰\n\n Showing last {1}.\n\n Navigate to the prev page to view previous {1}.\n\n{2}";

    switch (i) {
        case 1 :
            partKeyMap.push({
                key: 1,
                partValue: utils.format(firstPageNavigation, heading.toUpperCase(), paginatedText, userHistoryText)
            });
            break;
        case maxLength :
            partKeyMap.push({
                key: i,
                partValue: utils.format(lastPageNavigation, heading.toUpperCase(), paginatedText, userHistoryText)
            });
            break;
        default :
            partKeyMap.push({
                key: i,
                partValue: utils.format(nextPageNavigation, heading.toUpperCase(), paginatedText, userHistoryText)
            });
            break;
    }
}

/**
 *
 * Split long text to be paginated
 *
 * */
function paginationText(longText, paginatedText) {
    let i = 0;
    let partKeyMap = [];
    var n = 5; // chunk size

    var arrLength = longText.length; // Need to store this as splice will change the arrray length
    for (let k = 0;i<arrLength/n;k++){
        i++;
        let shortText = longText.splice(0, n);
        let userHistoryText = shortText.reduce((acc, val) => {
            return acc + val;
        }, "");

        const maxLength =  Math.ceil(arrLength/n);

        formatPartValue(i, paginatedText, partKeyMap, userHistoryText, maxLength);
    }
    return partKeyMap;
}


function getTextAfterPagination(msg, longText, paginatedText) {
    let callBackMap = [];
    const parts = paginationText(longText, paginatedText);
    const partValue = parts.find(p => p.key === Number(msg.data)).partValue;
    callBackMap.push({partValue: partValue, paginationValue: getPagination(parseInt(msg.data), parts.length)});
    return callBackMap;
}

function invalidCommand() {
    throw new Error("INVALID_COMMAND");
}


function isValidLongText(longText) {
    return !(longText == undefined);
}

function getCallBackQueryReply(msg, longText, paginatedText) {
    return isValidLongText(longText) ? getTextAfterPagination(msg, longText, paginatedText) : invalidCommand();
}

module.exports.getPagination = getPagination;
module.exports.paginationText = paginationText;
module.exports.getCallBackQueryReply = getCallBackQueryReply;
