
const config = {
    CHAIN_WSURI:    'wss://dex.binance.org/api/ws/',
    CHAIN_APIURI:   'https://dex.binance.org/',
    CHAIN_ADDRESS:  process.env.CHAIN_ADDRESS,
    CHAIN_PRIVKEY:  process.env.CHAIN_PRIVKEY,
    CHAIN_QRCODE:   process.env.CHAIN_QRCODE,
    EXPRESS_PORT:   process.env.PORT,
    MONGO_URL:      process.env.MONGO_URL,
    MONGO_DBNAME:   process.env.MONGO_DBNAME,
    TELEGRAM_TOKEN: process.env.TELEGRAM_TOKEN,
}

exports.config = config;