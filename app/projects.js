/**
 * Local Imports
 */
const utils = require('./utils');
const config = require('../config/config').config;


/**
 * Returns the project info
 * @return {String}              Formatted string.
 */
function getProjectInfo(ticker, callback){
    let coinId = config.botTokens.filter(x => x.ticker == ticker.toUpperCase()).map(x => x.id)[0]
    let projectData = config.botProjects.filter(x => x.coinid == coinId)[0]
    let outMsg = utils.format(config.botStrings.projectInfo,projectData.name,projectData.description, projectData.website, projectData.twitter, projectData.medium, projectData.telegram, coinId)
    return callback(outMsg.replace(/_/g, "\\_"))
}

/**
 * Returns the project news
 * @return {String}              Formatted string.
 */
function getProjectNews(ticker, callback){
    let coinId = config.botTokens.filter(x => x.ticker == ticker.toUpperCase()).map(x=>x.id)[0]
    let projectData = config.botProjects.filter(x => x.coinid == coinId)
    projectNews = projectData[0].updates;
    if(projectNews){
        projectNewsItem = projectNews[0].update
        let outMsg = utils.format(config.botStrings.projectNews,projectData[0].name,projectNewsItem)
        return callback(outMsg.replace(/_/g, "\\_"))
    } else {
        let outMsg = utils.format(config.botStrings.projectNews,projectData[0].name,"Looks like there isn't any news right now!")
        return callback(outMsg.replace(/_/g, "\\_"))
    }

}


/**
 * EXPORTS
 */
exports.getProjectInfo = getProjectInfo
exports.getProjectNews = getProjectNews