/**
 * External Imports
 */
const Telegraf = require('telegraf');
const Markup = require('telegraf/markup');
const Extra = require('telegraf/extra');


/**
 * Local Imports
 */
const utils = require('./utils');
const botUser = require('./users');
const tip = require('./tip');
const wallet = require('./wallet');
const markets = require('./markets');
const projects = require('./projects');
const config = require('../config/config').config;
const pageUtil = require('./paginationutil');
const session = require('telegraf/session');


/**
 * Constants
 */
const bot = new Telegraf(config.TELEGRAM_TOKEN);
bot.use(session());

/**
 * Telegram bot error handling
 */
bot.catch((err) => {
    console.warn('TELEGRAM ERR: ', err);
});


/**
 * Telegram inline menus
 */
function makeOptionsMenu(project){
  let optionsMenu = [Markup.callbackButton('📗 INFO', 'info-'+project),
  Markup.callbackButton('🔊 NEWS', 'news-'+project),
  Markup.callbackButton('📊 STATS', 'coin-'+project)];
  return optionsMenu;
}

function makeCoinsMenu(prefix,addMenu){
  let menuArray = [];

  if(addMenu){
    for (let i = 0, len = addMenu.length; i < len; i++) {
      menuArray.push(addMenu[i]);
    }
  }

  for (let i = 0, len = config.botTokens.length; i < len; i++) {
    let thisMenu = Markup.callbackButton(config.botTokens[i].ticker,prefix+'-'+config.botTokens[i].ticker.toLowerCase());
    menuArray.push(thisMenu);
  }

  menuArray.push(Markup.callbackButton('✅ Clear', 'delete'));
  return Markup.inlineKeyboard(menuArray,{columns:3});
}

const clearButton = Markup.inlineKeyboard([
  Markup.callbackButton('✅ Clear', 'delete')
]);

const depositMenu = Markup.inlineKeyboard([
  [Markup.callbackButton('⚠️ Notice', 'deposit-notice'),
  Markup.callbackButton('✅ Instructions', 'deposit-instruction')],
 [Markup.callbackButton('📮 Address & QR Code', 'depositaddress'),
  Markup.callbackButton('✅ Clear Message', 'delete')]
]);

const depositConf = Markup.inlineKeyboard([
  [Markup.callbackButton('👽 My Info', 'userinfo'),
  Markup.callbackButton('💳 Transaction History', 'txhistory')],
  [Markup.callbackButton('✅ Clear Message', 'delete')]
]);

const tipConf = Markup.inlineKeyboard([
  [Markup.callbackButton('👽 My Info', 'userinfo'),
  Markup.callbackButton('📆 Tip History', 'tiphistory')],
  [Markup.callbackButton('✅ Clear Message', 'delete')]
]);


/**
 * Telegram Bot Listeners
 */

bot.action('delete', ({ deleteMessage }) => {
  deleteMessage()
  .then()
  .catch(err => {console.log(err);});
});

bot.action(/^[info]+(-[a-z]+)?$/, ctx => {
  let project = ctx.match[1].split('-')[1];
  projects.getProjectInfo(project, outMsg => {
    ctx.editMessageText(outMsg, Extra.markup(makeCoinsMenu('info',makeOptionsMenu(project))) .webPreview(true) .markdown(true))
    .catch(err => {});
  });
});

bot.action(/^[news]+(-[a-z]+)?$/, ctx => {
  let project = ctx.match[1].split('-')[1];
  projects.getProjectNews(project, outMsg => {
    ctx.editMessageText(outMsg, Extra.markup(makeCoinsMenu('news',makeOptionsMenu(project))) .webPreview(true) .markdown(true))
    .catch(err => {});
  });
});

bot.action(/^[coin]+(-[a-z]+)?$/, ctx => {
  let project = ctx.match[1].split('-')[1];
  markets.getCoinStats(project, outMsg => {
    ctx.editMessageText(outMsg, Extra.markup(makeCoinsMenu('coin',makeOptionsMenu(project))) .webPreview(true) .markdown(true))
    .catch(err => {});
  });
});

bot.action(/^[deposit]+(-[a-z]+)?$/, ctx => {
  let instruction = ctx.match[1].split('-')[1];
  let msgid = ctx.update.callback_query.message.message_id
  let chatid = ctx.update.callback_query.message.chat.id;
  ctx.editMessageText(utils.format(config.botStrings['deposit_'+instruction]), Extra.markup(depositMenu) .webPreview(false) .markdown(true))
  .catch(err => {});
})

bot.action('depositaddress', ctx =>{
  let caption = "*DEPOSIT ADDRESS* 📮📮📮\n\n*Copy/Paste: *`"+config.CHAIN_ADDRESS+"`\n\n\
*Binance Explorer: *["+config.CHAIN_ADDRESS+"](https://explorer.binance.org/address/"+config.CHAIN_ADDRESS+")\n\n";
  ctx.replyWithPhoto({ url: config.CHAIN_QRCODE }, Extra.markup(clearButton) .caption(caption)  .webPreview(true) .markdown(true));
});

/**
 * Telegram Bot Listeners
 */
bot.command('start', ({ reply }) => {
    return reply(config.botStrings.welcome, Markup.keyboard(config.botStrings.startMenu) .oneTime() .resize() .extra());
});

bot.hears(['about','⚡️ About'], ctx => {
  botUser.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(config.botStrings.about,Extra.markup(clearButton) .webPreview(false));
});

bot.command(['about'], ctx => {
  botUser.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(config.botStrings.about,Extra.markup(clearButton) .webPreview(false));
});

bot.hears(['commands','🤖 Commands'], ctx => {
  botUser.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(config.botStrings.commands,Extra.markup(clearButton) .webPreview(false));
});

bot.hears(['info', 'ℹ️ Project Info, News & Stats'], ctx => {
  botUser.addUserIfNew(ctx.from)
  projects.getProjectInfo("rune", outMsg => {
    ctx.replyWithMarkdown(outMsg, Extra.markup(makeCoinsMenu("info",makeOptionsMenu("rune"))) .webPreview(true));
  });
});

bot.command(['projects'], ctx => {
  botUser.addUserIfNew(ctx.from)
  markets.getCoinStats('rune', outMsg => {
    ctx.replyWithMarkdown(outMsg, Extra.markup(makeCoinsMenu("info",makeOptionsMenu("rune"))) .webPreview(true));
  });
});

bot.command(['address'], ctx => {
  botUser.addUserIfNew(ctx.from)
  botUser.updateAddress(ctx.message, outMsg => {
    ctx.replyWithMarkdown(outMsg);
  });
});

bot.command(['tip'], ctx => {
  botUser.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(utils.format(config.botStrings.pending_tip), Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false))
  .then(res => {
    tip.tipRequest(ctx.message, outMsg => {
      bot.telegram.editMessageText(res.chat.id,res.message_id,null,outMsg,Extra.markup(tipConf) .markdown(true) .webPreview(false));
    });
  });
});

bot.command(['withdraw'], ctx => {
  botUser.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(utils.format(config.botStrings.pending_withdrawal), Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false))
  .then(res => {
    wallet.withdrawTips(ctx.message, outMsg => {
      bot.telegram.editMessageText(res.chat.id,res.message_id,null,outMsg,Extra.markup(depositConf) .markdown(true) .webPreview(false));
    });
  });
});

bot.hears(['🤑 Withdraw'], ctx => {
  botUser.addUserIfNew(ctx.from)
  botUser.getUserData(ctx.message.from, outMsg => {
      ctx.replyWithMarkdown(utils.format(config.botStrings.help_withdraw), Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false));
  });
});

bot.hears(['/me','👽 My Info & Balances'], ctx => {
  botUser.addUserIfNew(ctx.from)
  botUser.getUserData(ctx.message.from, outMsg => {
      ctx.replyWithMarkdown(outMsg, Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false));
  });
});

bot.action('userinfo', ctx => {
  botUser.addUserIfNew(ctx.from)
  botUser.getUserData(ctx.update.callback_query.from, outMsg => {
    ctx.replyWithMarkdown(outMsg, Extra.markup(clearButton) .webPreview(false));
  });
});

bot.action('txhistory', ctx => {
  botUser.addUserIfNew(ctx.from)
  wallet.getTransactions(ctx)
  .then(data => {
    let pagination = paginateThis(ctx, "transactions", data)
    ctx.replyWithMarkdown(pagination.partValue, Extra.markup(pagination.partKb) .webPreview(false))
    .catch(err => {});
  })
  .catch(error => {
    ctx.replyWithMarkdown(utils.sexyError("TIP_NOTRANSACTIONS"), Extra.markup(clearButton) .webPreview(false))
  })
});

bot.action('tiphistory', ctx => {
  botUser.addUserIfNew(ctx.from)
  tip.getTipHistory(ctx)
  .then(data => {
    let pagination = paginateThis(ctx, "tips", data)
    ctx.replyWithMarkdown(pagination.partValue, Extra.markup(pagination.partKb) .webPreview(false))
    .catch(err => {});
  })
  .catch(error => {
    console.log(error)
    ctx.replyWithMarkdown(utils.sexyError("TIP_NOTRANSACTIONS"), Extra.markup(clearButton) .webPreview(false))
  })
});

bot.hears(['/deposit','💰 Deposit'], ctx => {
  botUser.addUserIfNew(ctx.from)
  ctx.replyWithMarkdown(utils.format(config.botStrings.deposit_notice), Extra.markup(depositMenu) .inReplyTo(ctx.message.message_id) .webPreview(false));
});

function clearSessionVariables(ctx) {
    ctx.session.paginatedText = [];
    ctx.session.paginationType = [];
}

const paginateThis = (ctx, type, data) => {
  clearSessionVariables(ctx);
  ctx.session.paginationType.push({id : ctx.from.id, pageType: JSON.stringify(utils.paginationText[type])});
  ctx.session.paginatedText.push({id : ctx.from.id, longText: JSON.stringify(data)});
  const parts = pageUtil.paginationText(data, JSON.stringify(utils.paginationText[type]));
  const thisPart = {};
  thisPart.partValue = parts.find(p => p.key === 1).partValue;
  thisPart.partKb = pageUtil.getPagination(1, parts.length);
  return thisPart;
}

/**
 *
 * txhistory with pagination
 *
 * */
bot.hears(['/mytx','💳 Tx History'], ctx => {
    botUser.addUserIfNew(ctx.from)
    wallet.getTransactions(ctx.message)
    .then(data => {
      let pagination = paginateThis(ctx, "transactions", data)
      ctx.replyWithMarkdown(pagination.partValue, Extra.markup(pagination.partKb) .inReplyTo(ctx.message.message_id) .webPreview(false))
      .catch(err => {});
    })
    .catch(error => {
      ctx.replyWithMarkdown(utils.sexyError("TIP_NOTRANSACTIONS"), Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false))
    })
});

/**
 *
 * tiphistory with pagination
 *
 * */
bot.hears(['/mytips', '📆 Tip History'], ctx => {
  tip.getTipHistory(ctx.message)
  .then(data => {
    let pagination = paginateThis(ctx, "tips", data)
    ctx.replyWithMarkdown(pagination.partValue, Extra.markup(pagination.partKb) .inReplyTo(ctx.message.message_id) .webPreview(false))
    .catch(err => {});
  })
  .catch(error => {
    console.log(error)
    ctx.replyWithMarkdown(utils.sexyError("TIP_NOTRANSACTIONS"), Extra.markup(clearButton) .inReplyTo(ctx.message.message_id) .webPreview(false))
  })
});

/**
 *
 * Call back query is executed on the click on button og pages during pagination
 *
 * */
bot.on(['callback_query'], ctx => {
    try {
        const msg = ctx.update.callback_query;
        const sessionText = ctx.session.paginatedText.find(pageText => pageText.id === ctx.from.id);
        const pageType = ctx.session.paginationType.find(pageType => pageType.id === ctx.from.id);
        const longText = JSON.parse(sessionText.longText);
        const paginationType = JSON.parse(pageType.pageType);
        var replyMessage = pageUtil.getCallBackQueryReply(msg, longText, paginationType)[0];
        ctx.editMessageText(replyMessage.partValue, Extra.markup(replyMessage.paginationValue).webPreview(false).markdown(true));
    }
    catch(error) {
        ctx.replyWithMarkdown(utils.sexyError(error.message), Extra.markup(clearButton).webPreview(false));
    }
});

/**
 * EXPORTS
 */
exports.bot = bot;
exports.Extra = Extra;
exports.depositMenu = depositMenu;
exports.depositConf = depositConf;
