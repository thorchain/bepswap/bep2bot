/**
 * External Imports
 */
const MongoClient = require('mongodb').MongoClient;


/**
 * Local Imports
 */
const utils = require('./utils');
const config = require('../config/config').config;
   

/**
 * Returns a collection from db
 * @param  {String} collectionName  The name of the collection.
 * @return {Object}                 collection from db
 */
function loadCollection(collectionName) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;
            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection(collectionName);
            collection.find({}).toArray()
                .then(response => resolve(response))
                .catch(err => reject(new Error(err)))
        });
    });
}

/**
 * Adds a user to the database
 * @param  {Object} userData    The new user object (@id , @username etc.)
 * @return {Object}               collection from db
 */
function dbAddUser(userData) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('users');

            collection.insertOne(userData)
                .then(response => resolve(userData))
                .catch(err => {
                    let error = new Error("DB_INSERT_FAIL")
                    reject(error)
                })
        })
    });
}


/**
 * Updates a users balance
 * @param  {Number} userId   The telegram user Id
 * @param  {Number} amt      The amount of then transaction
 * @return {Object}          The resultant user object
 */
function dbUpdateBalance(userInfo, amt, tkn) {
    return new Promise((resolve, reject) => {
        var query = utils.hasToken(userInfo,tkn) ? {userid: userInfo.userid,'balances.token': tkn} : {userid: userInfo.userid}
        var update = utils.hasToken(userInfo,tkn) ? {$inc: {'balances.$.balance': amt}} : {$addToSet: {balances: {token: tkn, balance:amt}}}

        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('users');
            
            collection.findOneAndUpdate(query,update,{returnOriginal: false})
                .then(response => {
                    if (!response.value) {
                        let error = new Error("DEPOSIT_BADTOKEN")
                        utils.winston.warn("TOKEN INCREMENT FAILED. TOKEN NOT FOUND IN BALANCES.")
                        reject(error)
                    } else {
                        resolve(response.value)
                    }
                })
                .catch(err => reject(new Error(err)))
        });
    });
}


/**
 * Updates a BEP address
 * @param  {Number} userId          The telegram user Id
 * @param  {String} bepAddress      The address to be added
 * @return {Object}                 The resultant user object
 */
function dbUpdateAddress(userId, bepAddress) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('users');

            collection.findOneAndUpdate({userid: userId}, {$set: {bepaddress: bepAddress}}, {returnOriginal: false})
                .then(response => resolve(response.value))
                .catch(err => {
                    let error = new Error("DB_INSERT_FAIL")
                    utils.winston.warn("ADDRESS: COULDN'T UPDATE USERS ADDRESS")
                    reject(error)
                })
        })
    });
}


/**
 * Updates the transaction ledger
 * @param  {Object} userObj     The telegram user object
 * @param  {Number} amt         The amount of the transaction
 * @param  {string} hash        The hash of the transaction
 * @return {Object}             An object containing the ledger for the user
 */
function dbUpdateLedger(userObj, amt, tkn, hash) {
    var myDate = new Date();

    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('ledger');

            var ledgerEntry = {
                date: myDate,
                userid: userObj.userid,
                amount: amt,
                token: tkn,
                txhash: hash,
                direction: amt < 0 ? "out" : "in"
            }

            collection.insertOne(ledgerEntry)
                .then(response => resolve(ledgerEntry))
                .catch(err => reject(new Error(err)))
        })
    });
}


/**
 * Updates the tip history
 * @param  {Object} tipperObj       The telegram user object for tipper
 * @param  {Object} tippeeObj       The telegram user object for tippee
 * @param  {string} amt             The amount of the tip
 * @return {Object}                 An object containing the history entry
 */
function dbUpdateTipHistory(tipperObj, tippeeObj, amt, token) {
    var myDate = new Date();

    return new Promise((resolve, reject) => {
        MongoClient.connect(config.MONGO_URL, {
            useNewUrlParser: true
        }, function (err, client) {
            if (err) throw err;

            db = client.db(config.MONGO_DBNAME);
            var collection = db.collection('tiphistory');

            var tipEntry = {
                date: myDate,
                tipper: tipperObj.userid,
                tippee: tippeeObj.userid,
                tippername: tipperObj.username,
                tippeename: tippeeObj.username,
                amount: amt,
                token: token
            }

            collection.insertOne(tipEntry)
                .then(response => resolve(tipEntry))
                .catch(err => {
                    let error = new Error("DB_INSERT_FAIL")
                    reject(error)
                })
        })
    });
}


/**
 * EXPORTS
 */
exports.loadCollection = loadCollection
exports.dbAddUser = dbAddUser
exports.dbUpdateAddress = dbUpdateAddress
exports.dbUpdateBalance = dbUpdateBalance
exports.dbUpdateLedger = dbUpdateLedger
exports.dbUpdateTipHistory = dbUpdateTipHistory