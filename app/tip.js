/**
 * Local Imports
 */
const config = require('../config/config').config;
const utils = require('./utils');
const mongo = require('./mongo');
const users = require('./users');


/**
 * Tip request from user
 * @param  {Object} messageObj       The raw message object from telegram
 * @return {String}                  The message back to the user.
 */
function tipRequest(messageObj, callback){

    // parse the raw telegram message and get back a nice object
    let tipObj = utils.getTipObj(messageObj);

    if(!tipObj || tipObj.message){
        let outMsg = utils.format(config.botStrings.error_tip,utils.sexyError(tipObj.message))
        return callback(outMsg);
    } else {

    // validate & process the tip
    validateTip(tipObj)
        .then(result => {
            processTip(utils.grabUser(tipObj.from), utils.grabUser(tipObj.to), tipObj.amount, utils.tickerToToken(tipObj.token), function(error,result){
                let outMsg = error ? utils.format(config.botStrings.error_tip,utils.sexyError(error.message)) : result
                return callback(outMsg)
            })

        })
        .catch(error => {
            let outMsg = utils.format(config.botStrings.error_tip,utils.sexyError(error.message))
            return callback(outMsg)
        })
    }
}


/**
 * Validates a tip request
 * @param  {Object} withdrawRequest     An object containing the users info & command arguments
 * @return {Object} validationArray     An array of validation promises
 */

const validateTip = async(tipRequest) => {
    let isUser = utils.isUser(tipRequest.from)
    let isUserTippee = utils.isUser(tipRequest.to)
    let isNumber = utils.isNumber(tipRequest.amount)
    let isAboveZero = utils.isAboveZero(tipRequest.amount)
    let isValidToken = utils.isValidToken(tipRequest.token)
    let hasBalance = utils.hasBalance(utils.grabUser(tipRequest.from), utils.tickerToToken(tipRequest.token), tipRequest.amount)
    let validationArray = await Promise.all([isUser, isUserTippee, isNumber, isAboveZero, isValidToken, hasBalance])
    return validationArray
}


/**
 * Processess a tip after validation has been completed
 * @param  {Object} tipper          The bot user object for the tipper
 * @param  {Object} tippee          The bot user object for the tippee
 * @param  {Number} amount          The amount of tokens to withdraw
 * @param  {String} token           The BEP2 token to withdraw eg. RUNE-B1A
 * @return {String}                 String to return to the user via msg
 */
const processTip = (tipper, tippee, amount, token, callback) => {
    let debitAmt = Math.abs(amount) * -1;
    Promise.all([
        users.updateBalance(tipper,debitAmt,token),
        users.updateBalance(tippee,amount, token)
    ])
    .then(res => {
        updateTipHistory(tipper, tippee, amount, token)
        .then(tipEntry => {
            let tipperNewBalance = res[0].balances.filter(x => x.token == token).map(x => x.balance)[0]
            let tippeeNewBalance = res[1].balances.filter(x => x.token == token).map(x => x.balance)[0]
            let outMsg = utils.format(config.botStrings.success_tip,amount,utils.tokenToTicker(token), tippee.username, tippeeNewBalance, tipperNewBalance)
            return callback(null,outMsg);
        })
        .catch(error => {
            let outMsg = utils.format(config.botStrings.error_tip,utils.sexyError(error.message))
            return callback(outMsg);
        })
    })
    .catch(error => {
        let outMsg = utils.format(config.botStrings.error_tip,utils.sexyError(error.message))
        return callback(outMsg)
    })
}


/**
 * Updates the tip history
 * @param  {Object} tipperObj       The telegram user object tipper
 * @param  {Object} tippeeObj       The telegram user object tippee
 * @param  {string} amount          The amount of the tip
 * @param  {string} token           The token for the tip
 * @return {Object}                 An object containing the history entry
 */
function updateTipHistory(tipperObj, tippeeObj, amount, token){
    return new Promise((resolve, reject) => {
        mongo.dbUpdateTipHistory(tipperObj, tippeeObj, amount, token)
        .then(tipEntry => {
            config.botHistory.push(tipEntry)
            resolve(tipEntry)
        })
        .catch(error => {
            utils.winston.warn("TIP: FAILED TO UPDATE TIP HISTORY DB.")
            reject(error);
        })
    })
}


/**
 * Returns the tip history data for a user
 * @param  {Object} messageObj      The telegram message object
 * @return {Object}                 Message String
 */
function getTipHistory(messageObj, callback){
    return new Promise((resolve, reject) => {
        let userInfo = utils.grabUser(messageObj.from)

        if(!userInfo){
            let error = new Error("USER_NOTREG")
            reject(error)
        }

        let rawData = config.botHistory.filter(x => x.tippee == userInfo.userid || x.tipper == userInfo.userid);
        let userTipHistory = utils.multisort(rawData,['date'], ['DESC']);

        if(!userTipHistory || userTipHistory.length == 0) {
            let error = new Error("TIP_NOHISTORY")
            reject(error)
        } else {
            let userHistoryData = userTipHistory.map(x => utils.formatTipHistory(x, messageObj.from.id));
            resolve(userHistoryData);
        }
    })
}

/**
 * EXPORTS
 */
exports.tipRequest = tipRequest;
exports.getTipHistory = getTipHistory;
