/**
 * Local Imports
 */
const config = require('../config/config').config;
const format = require('string-format');
const winston = require('winston');
const binance = require('./binance')

winston.add(new winston.transports.File({ filename: 'logfile.log' }))
winston.add(new winston.transports.Console())

var paginationText = {};
paginationText.tips = "tips";
paginationText.transactions = "transactions";

const errorList = {
  INVALID_COMMAND: "📆 Whoops invalid command",
  ADDR_NOADDR:"You didn't provide a valid binance chain address.",
  ADDR_NOTVALID:"You need to provide a valid binance chain address.",
  ADDR_INUSE:"You can't share an address with someone else.",
  USER_NOTREG:"You need to be registered for that. Start the bot `/start` in a private message to register.",
  USER_UNKNOWN: "The username you gave me isn't registered.",
  ERR_UNKNOWN:"I has the dumb! Some unknown error happen daddy!",
  DB_FAIL: "There was an issue updating the database. Try again later",
  DB_INSERT_FAIL: "There was an issue updating the database. Try again later",
  USERNAME_INVALID: "The username was invalid or not supplied.",
  TIP_UNKNOWN: "There was an error getting user details. Please check the tippee is registered.",
  TIP_NOBALANCE: "You have insufficient balance for this tip!",
  TIP_INVALIDTOKEN:"This isn't a supported BEP2 token.",
  TIP_INVALIDAMT:"You haven't specified an amount, or the amount isn't correct.",
  TIP_NOHISTORY:"📆 Whoops no history! Better get tipping!",
  TIP_NOTRANSACTIONS:"📆 Whoops no transaction history!",
  TIP_BADINSTRUCTION:"You didn't provide the right instructions.",
  WITHDRAW_BADINSTRUCTION:"You didn't provide the right instructions. You need to provide the token and amount you want to withdraw.",
  WITHDRAW_NOBALANCE: "You don't have sufficient balance to withdraw",
  WITHDRAW_INVALIDTOKEN:"This isn't a supported BEP2 token.",
  WITHDRAW_INVALIDAMT:"You haven't specified an amount, or the amount isn't correct.",
  WITHDRAW_NOADDR:"You need to add a binance chain address before you can withdraw. Type `/address <youraddress>` to add.",
  DEPOSIT_BADTOKEN: "Bad token",
  TOKEN_INVALID: "The supplied token isn't recognised or supported.",
  BALANCE_INSUFFICIENT: "Your balance is insufficient to complete this transation.",
  AMOUNT_INVALID: "The amount is invalid or not supplied."
}

format.extend(String.prototype, {})

/**
 * Sorts object by columns
 * @param   {Object} arr        Array
 * @param   {Object} columns    Array of columns eg ['col','col']
 * @param   {Object} order_by   Array of sorting eg ['asc','desc']
 * @return  {Object}            New sorted array *
 */
  multisort = function(arr, columns, order_by) {
    if(typeof columns == 'undefined') {
        columns = []
        for(x=0;x<arr[0].length;x++) {
            columns.push(x);
        }
    }

    if(typeof order_by == 'undefined') {
        order_by = []
        for(x=0;x<arr[0].length;x++) {
            order_by.push('ASC');
        }
    }

    function multisort_recursive(a,b,columns,order_by,index) {
        var direction = order_by[index] == 'DESC' ? 1 : 0;
        var is_numeric = !isNaN(+a[columns[index]] - +b[columns[index]]);
        var x = is_numeric ? +a[columns[index]] : a[columns[index]].toLowerCase();
        var y = is_numeric ? +b[columns[index]] : b[columns[index]].toLowerCase();
        if(x < y) {
                return direction == 0 ? -1 : 1;
        }
        if(x == y)  {
            return columns.length-1 > index ? multisort_recursive(a,b,columns,order_by,index+1) : 0;
        }
        return direction == 0 ? 1 : -1;
    }

    return arr.sort(function (a,b) {
        return multisort_recursive(a,b,columns,order_by,0);
    });
  }


/**
 * Takes a /tip message object & returns a neat object with all tip information for validation
 * @param   {Object} messageObj   Message Object
 * @return  {Object}              Tip Object
 */
function getTipObj(messageObj){
  let tipObj = getPartiesFromMsg(messageObj)
  let userCommand = splitMsg(messageObj.text)

  if(tipObj){
    // offset if the user was replying & therefore didn't include a @username arg.
    let x = tipObj.reply ? 0 : 1;
    tipObj.amount = parseInt(userCommand.args[x]) || false
    tipObj.token = userCommand.args[x+1] || false
    return tipObj;
  } else {
    return new Error("TIP_BADINSTRUCTION")
  }
 }

 /**
 * Taskes a users /withdrawal request message and returns a neat object with amount, token, from etc.
 * @param   {Object} messageObj   Message Object
 * @return  {Object}              Withdrawal Object
 */
function getWithdrawObj(messageObj){
  let usrCmd = splitMsg(messageObj.text)
  let withdrawObj = {}

  if(usrCmd && usrCmd.args.length == 2 ){
    withdrawObj.amount = parseInt(usrCmd.args[0])
    withdrawObj.token = usrCmd.args[1]
    withdrawObj.from = messageObj.from
    return withdrawObj;
  } else {
    return new Error("WITHDRAW_BADINSTRUCTION")
  }
 }


 /**
 * Takes a telegram message object and returns an object containing sender & recipient
 * Required because a user can tip by replying to a message or by mentioning a @username
 * @param   {Object} messageObj Message Object
 * @return  {Object}            msgParties Object
 */
function getPartiesFromMsg(messageObj){
  let msgParties = {};

  // The tipper is replying to the tippees message. eg. /tip 100
  if(messageObj.reply_to_message){
    msgParties.to = messageObj.reply_to_message.from
    msgParties.from = messageObj.from
    msgParties.reply = true;
    return msgParties;

  // The tipper is calling directly eg. /tip @user 100
  } else {
    let userCommand = splitMsg(messageObj.text)
    let userMention = userCommand.args[0] ? userCommand.args[0] : false

    if (userMention){
      msgParties.to = userMention;
      msgParties.from = messageObj.from
      msgParties.reply = false;
      return msgParties;
    } else {
      return false;
    }
  }
 }


 /**
 * Returns users chat text into an object split by commands & params
 * @param   {Object} msg    Telegram Message Object
 * @return  {Object}        Object containing text, command & arguments
 */
  function splitMsg(msg){

    const text = msg
    if (text.startsWith('/')) {
      const match = text.match(/^\/([^\s]+)\s?(.+)?/)
      let args = []
      let command
      if (match !== null) {
        if (match[1]) {
          command = match[1]
        }
        if (match[2]) {
          args = match[2].split(' ')
        }
      }

      let msgObj = {
        raw: text,
        command,
        args
      }

      return msgObj || false;
    }
  }


/**
 * Takes a binance chain payload & returns a neat object.
* @param   {Object}   payload    Binance payload object
* @return  {Object}              Neat object
*/
function parseTransaction(payload){
  let tx = {}
  tx.txHash   = payload.data.H;
  tx.txTo     = payload.data.t[0].o;
  tx.txFrom   = payload.data.f;
  tx.txTkn    = payload.data.t[0].c[0].a;
  tx.txQty    = parseInt(payload.data.t[0].c[0].A);
  return tx;
}

/**
 * Returns the bots user data object from a telegram user object
 * @param   {Object}  userObj    Telegram user object (or username)
 * @return  {Object}             Our user data object
 */
const grabUser = (userObj) => {
  if(userObj && userObj.id) { userObj.userid = userObj.id}
  var searchById = userObj.hasOwnProperty("userid") ? true : false;
  let userInfo = config.botUsers.filter(x => searchById ? x.userid == userObj.userid : x.username == userObj.substring(1,userObj.length));
  return userInfo[0] ? userInfo[0] : false
}


/**
 * Other minor helper functions. Self explanatory
 */
const updateUserArray = (userInfo) => {
  config.botUsers = config.botUsers.filter(x => x.userid != userInfo.userid);
  config.botUsers.push(userInfo)
 }

const numberWithCommas = (num) => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const getChart = (num) => {
  return num < 0 ? '📉' : '📈';
}

const shortHash = (txHash) => {
  let newHash = txHash.substring(0,6)+"..."+txHash.substring(txHash.length - 6, txHash.length)
  return newHash;
}

const getUserByAddress = (chainAddress) => {
  return config.botUsers.filter(x => x.bepaddress == chainAddress)[0] || new Error("USER_UNKNOWN")
}

const tokenToTicker = (token) => {
  return config.botTokens.filter(x => x.token == token).map(x => x.ticker)[0]
}

const tickerToToken = (ticker) => {
  return  config.botTokens.filter(x => x.ticker == ticker.toUpperCase()).map(x => x.token)[0] || makeError("TOKEN_INVALID");
}

const getTokenBalance  = (userData, token) => {
  return userData.balances.filter(x => x.token == token).map(x => x.balance)[0]
}

const isNumber = (number) => {
  var numCheck = new RegExp(/^\d+$/)
  return numCheck.test(number) || makeError("AMOUNT_INVALID")
}

const isAboveZero = (number) => {
  var numCheck = new RegExp(/^\d+$/)
  return numCheck.test(number) && number > 0 ? true : makeError("AMOUNT_INVALID")
}

const makeError = (message) => {
  throw new Error(message);
}

const hasBalance = (userData, token, amount) => {
  return getTokenBalance(grabUser(userData),token)-amount >=0 ? true : makeError("BALANCE_INSUFFICIENT")
}

const hasToken = (userData, token) => {
  let balance = userData.balances.filter(x => x.token == token)
  return balance[0] ? true : false;
}

const hasAddress = (userData) => {
  return userData.bepaddress ? true : makeError("WITHDRAW_NOADDR")
}

const isUser = (userObj) => {
  return grabUser(userObj) ? true : makeError("USER_UNKNOWN")
}

const isValidToken = (ticker) => {
  return ticker
  ? config.botTokens.filter(x => x.ticker == ticker.toUpperCase()).map(x => x.token)[0] ? true :  makeError("TOKEN_INVALID")
  : makeError("TOKEN_INVALID")
}

const isValidAddress = (address) => {
  return binance.bnbClient.checkAddress(address) || makeError("ADDR_NOTVALID")
}

const isAddressAvailable = (address) => {
  let user = config.botUsers.filter(x => x.bepaddress == address)
  return !user[0] ? true : makeError("ADDR_INUSE")
}

const sexyError = (error) => {
  return errorList[error];
}

const sexyDate = (ISODate) => {
  return ISODate.toISOString().replace(/T/, ' ').replace(/\..+/, '')
}

const formatLedger = (lineItem) => {
  return lineItem.amount > 0 ?
  "📆 *"+ sexyDate(lineItem.date) +"*\n 📥 DEPOSIT "+ lineItem.amount + " [" + lineItem.token + "](https://explorer.binance.org/tx/"+lineItem.txhash+")\n\n" :
  "📆 *"+ sexyDate(lineItem.date) +"*\n 📤 WITHDRAW " + lineItem.amount + " [" + lineItem.token + "](https://explorer.binance.org/tx/"+lineItem.txhash+")\n\n";
}

const formatTipHistory = (tipItem, userId) => {
  return  tipItem.tippee == userId ?
    "📆 *"+ sexyDate(tipItem.date) +"\n*📥 "+tipItem.amount + " " + tipItem.token + " recevied from @"+tipItem.tippername+" \n\n":
    "📆 *" + sexyDate(tipItem.date) +"\n*📤 "+tipItem.amount + " " + tipItem.token + " sent to @"+tipItem.tippeename+" \n\n"
}

const prettyBalances = (userInfo) => {
  let userBalances = "";
  userInfo.balances.forEach(e => {
      if(tokenToTicker(e.token)){
        userBalances = userBalances + "▪ *"+tokenToTicker(e.token) + ":* " + e.balance + "\n"
      }
  });
  return userBalances
}

/**
 * EXPORTS
 */
exports.prettyBalances = prettyBalances
exports.formatTipHistory = formatTipHistory
exports.formatLedger = formatLedger
exports.sexyDate = sexyDate
exports.getTokenBalance = getTokenBalance
exports.isNumber = isNumber
exports.hasBalance = hasBalance
exports.isUser = isUser
exports.grabUser = grabUser
exports.isAboveZero = isAboveZero
exports.winston = winston
exports.splitMsg = splitMsg
exports.multisort = multisort
exports.numberWithCommas = numberWithCommas
exports.getPartiesFromMsg = getPartiesFromMsg
exports.getTipObj = getTipObj
exports.sexyError = sexyError;
exports.format = format;
exports.getChart = getChart;
exports.getWithdrawObj = getWithdrawObj
exports.shortHash = shortHash
exports.tokenToTicker = tokenToTicker
exports.updateUserArray = updateUserArray
exports.getUserByAddress = getUserByAddress
exports.parseTransaction = parseTransaction
exports.hasAddress = hasAddress
exports.isValidToken = isValidToken
exports.tickerToToken = tickerToToken
exports.paginationText = paginationText
exports.isValidAddress = isValidAddress
exports.isAddressAvailable = isAddressAvailable
exports.hasToken = hasToken