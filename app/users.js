/**
 * Local Imports
 */
const config = require('../config/config').config;
const mongo = require('./mongo');
const utils = require('./utils');
const telegram = require('./telegram')
const binance = require('./binance')


/**
 * Returns the formatted user information for telegram presentation
 * @param  {Object} userObj      The telegram userid
 * @return {String}              Formatted string.
 */
function getUserData(userObj, callback){
    let userInfo = utils.grabUser(userObj)
    return userInfo
    ? callback(utils.format(config.botStrings.userInfo,userInfo.status,userInfo.userid,userInfo.username,userInfo.bepaddress, utils.prettyBalances(userInfo)))
    : callback(utils.format(config.botStrings.error_userInfo,utils.sexyError("USER_NOTREG")))
}


/**
 * Adds a new telegram user to the bot
 * @param  {Object} userObj     The telegram user object
 * @return {Object}             User object (id, username, balance etc)
 */
function addUser(userObj){
    return new Promise((resolve, reject) => {
        var myDate = new Date();

        var userData = {
            datejoined:myDate,
            userid:userObj.id,
            username:userObj.username ? userObj.username : "unknown",
            bepaddress:"",
            role:"user",
            status:"registered",
            balances:[
                {token: "RUNE-B1A",balance:0},
            ]

        }

        mongo.dbAddUser(userData)
            .then(newUser => {
                config.botUsers.push(newUser)
                resolve(newUser)
            })
            .catch(err => {
                let error = new Error("DB_FAIL")
                reject(error);
            })
    });
}


/**
 * Checks if user is existing, otherwise adds a new user
 * @param  {Object} userObj         The telegram user object
 * @return {Object}                 User object (id, username, balance etc)
 */
function addUserIfNew(userObj){
    try {
        utils.isUser(userObj) 
    } catch (error) {
        addUser(userObj)
    }
}

/**
 * Validates address update
 * @param  {Object} userData            User Data 
 * @param  {Object} address             Bep Address to update
 * @return {Object}                     An array of validation promises
 */

const validateAddress = async(userData, address) => {
    let isUser = utils.isUser(userData)
    let isValidAddress = utils.isValidAddress(address)
    let isAddressAvailable = utils.isAddressAvailable(address)
    let validationArray = await Promise.all([isUser, isValidAddress, isAddressAvailable])
    return validationArray
}

/**
 * Adds/Updates the users bepaddress
 * @param  {Object} messageObj      The telegram message object
 * @return {Object}                 User object (id, username, balance etc)
 */
function updateAddress(messageObj, callback){
    let splitMsg = messageObj.text ? utils.splitMsg(messageObj.text) : false
    let bepAddress = splitMsg && splitMsg.args[0] ? splitMsg.args[0] : false

    validateAddress(messageObj.from, bepAddress)
    .then(result => {
        mongo.dbUpdateAddress(messageObj.from.id, bepAddress)
        .then(userData => {
            utils.updateUserArray(userData)
            return callback("Update successful.")
        })
        .catch(error => {
            let outMsg = utils.sexyError(error.message);
            return callback(outMsg)
        })
    })
    .catch(error => {
        let outMsg = utils.sexyError(error.message);
        return callback(outMsg)
    })
}



/**
 * Updates a users balance
 * @param  {Number}     userId   The telegram user Id
 * @param  {Number}     amt      The amount of then transaction
 * @param  {String}     token    The token to update
 * @return {Object}              The resultant user object
 */
function updateBalance(userInfo, amt, token){
    return new Promise((resolve, reject) => {
    mongo.dbUpdateBalance(userInfo, amt, token)
        .then(userData => {
            utils.updateUserArray(userData);
            resolve(userData)
        })
        .catch(err => {
            console.log(err)
            utils.winston.warn("BALANCE: FAILED TO UPDATE USERS BALANCE")
            let error = new Error("DB_FAIL")
            reject(error);
        })
    })
}


/**
 * Sends a message proactively to a known user
 * @param  {Object} userId        Telegram userid
 */
function messageUser(userid, outMsg, menu){
    let userData = config.botUsers.filter(x => x.userid == userid);
    if(!userData || !userData[0]){
        let error = new Error("USER_UNKNOWN")
        return false;
    } else {
        telegram.bot.telegram.sendMessage(userid, outMsg, telegram.Extra.markup(telegram.depositConf) .webPreview(false) .markdown(true))
        return true;
    }
}


/**
 * EXPORTS
 */
exports.addUserIfNew = addUserIfNew;
exports.addUser = addUser;
exports.updateAddress = updateAddress;
exports.updateBalance = updateBalance;
exports.getUserData = getUserData;
exports.messageUser = messageUser