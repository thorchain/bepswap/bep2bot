/**
 * External Imports
 */
const axios = require('axios');


/**
 * Local Imports
 */
const utils = require('./utils');
const config = require('../config/config').config;


/**
 * Gets market data for tokens
 * @param   {String}     CoinId from coingecko
 * @return  {Object}     Object of market data.
 */
function loadGeckoData(coinId){
    let url = 'https://api.coingecko.com/api/v3/coins/'+coinId+'?localization=false&tickers=false&market_data=true&community_data=true&developer_data=false&sparkline=false';
    return new Promise((resolve, reject) => {
        axios.get(url)
        .then(res => {
            if(res && res.data){
                let marketData = res.data;
                resolve(marketData);
            } else {
                let error = new Error("API_FAIL");
                reject(error);
            }
        })
        .catch(err => {
            let error = new Error("API_FAIL");
            reject(error);
        });
    });
}


/**
 * Load all coin data
 * @return {Object}     Object of market data.
 */
function loadMarketData(){
    let projectlist = config.botProjects.map(x => x.coinid);
    config.coinData = {};
    for (let index = 0; index < projectlist.length; index++) {
        let coinId = projectlist[index];
        config.coinData[coinId] = {};
        loadGeckoData(coinId)
            .then(coinData => {
                config.coinData[coinId].symbol = coinData.symbol.toUpperCase();
                config.coinData[coinId].rank = coinData.market_cap_rank;
                config.coinData[coinId].price = (coinData.market_data.current_price.usd).toFixed(4);
                config.coinData[coinId].mcap = utils.numberWithCommas(coinData.market_data.market_cap.usd);
                config.coinData[coinId].vol =  utils.numberWithCommas(coinData.market_data.total_volume.usd);
                config.coinData[coinId].supply = utils.numberWithCommas(coinData.market_data.total_supply.toFixed(0));
                config.coinData[coinId].circsupply = utils.numberWithCommas(coinData.market_data.circulating_supply.toFixed(0));
                config.coinData[coinId].change1d = parseFloat(coinData.market_data.price_change_percentage_24h.toFixed(2));
                config.coinData[coinId].change7d = parseFloat(coinData.market_data.price_change_percentage_7d.toFixed(2));
            })
            .catch(err => {
                utils.winston.warn("MARKET DATA API FAILURE");
            });
      }
}


/**
 * Returns the coin stats info
 * @return {String}              Formatted string.
 */
function getCoinStats(ticker, callback){
    let coinId = config.botTokens.filter(x => x.ticker == ticker.toUpperCase()).map(x => x.id)[0]
    let coinData = config.coinData[coinId];
    let dchart = utils.getChart(coinData.change1d);
    let wchart = utils.getChart(coinData.change7d);
    let outMsg = utils.format(config.botStrings.coinStats,coinData.symbol,coinData.mcap,coinData.price,coinData.vol,coinData.supply,coinData.circsupply,dchart,coinData.change1d,wchart,coinData.change7d,coinId)
    return callback(outMsg)
}


/**
 * EXPORTS
 */
exports.loadGeckoData = loadGeckoData;
exports.loadMarketData = loadMarketData;
exports.getCoinStats = getCoinStats