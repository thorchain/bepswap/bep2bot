/**
 * Local Imports
 */
require('dotenv').config()

const config = require('./config/config').config
const mongo = require('./app/mongo');
const utils = require('./app/utils');
const telegram = require('./app/telegram');
const markets = require('./app/markets');
const express  = require('express');

/**
 * Populates all our data objects eg. users, ledger, strings.
 */
function loadCollections(){
    Promise.all([
        mongo.loadCollection('users'),
        mongo.loadCollection('projects'),
        mongo.loadCollection('ledger'),
        mongo.loadCollection('tiphistory'),
        mongo.loadCollection('strings')
    ]).then((res)=>{
        if(res.length==5) {
            config.botUsers=res[0];
            config.botProjects=res[1];
            config.botLedger=res[2];
            config.botHistory=res[3];
            config.botStrings=res[4][0];
            config.botTokens = config.botStrings.tokens
            utils.winston.info("COLLECTIONS LOADED")
            markets.loadMarketData()
        } else {
            utils.winston.info("COLLECTIONS DID NOT LOAD!");
        }
    })
}


/**
 * Runs all the functions necessary to start the bot.
 */
function startBot(){
    utils.winston.info("STARTING")
    let app = express();
    app.listen(config.EXPRESS_PORT, () => utils.winston.info(`EXPRESS LISTENING ON PORT ${config.EXPRESS_PORT}`));
    loadCollections()
    telegram.bot.launch();
}


/**
 * Start everything!
 */
startBot();
setInterval(markets.loadMarketData,300000)
